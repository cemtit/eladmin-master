package me.zhengjie.sql;


import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.InputStream;

@Component
public class DbManager {
    private static final String resource = "db/cemtdb.xml";
    private InputStream is;
    private SqlSessionFactory factory;
    private SqlSession sqlSession;

   @PostConstruct
    public void init() {
        try {

            is = Resources.getResourceAsStream(resource);//加载核心配置文件
            factory = new SqlSessionFactoryBuilder().build(is); //获得工厂对象
            sqlSession = factory.openSession();  //获取核心对象

        } catch (Exception e) {

        }
    }

   @PreDestroy
    public void destroy() {
        try {
            sqlSession.close();
        } catch (Exception e) {

        }
    }


    public int getTotalCount() {
        try {
              return sqlSession.selectOne("getCount");
        } catch (Exception e) {
            return 0;

        }
    }


}
